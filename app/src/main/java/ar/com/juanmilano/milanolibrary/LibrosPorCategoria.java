package ar.com.juanmilano.milanolibrary;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class LibrosPorCategoria extends AppCompatActivity {

    String libroElegido;
    ListView listLibros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libros_por_categoria);

        listLibros = (ListView)findViewById(R.id.listLibros);

        MiHelperBD helper = new MiHelperBD(this, "Biblioteca", null, 1);

        SQLiteDatabase bd = helper.getReadableDatabase();

        Intent intent = getIntent();
        final String libroTraido = intent.getStringExtra("categoria");

        Cursor c = bd.rawQuery("Select b.titulo, c.nombre from Biblioteca b inner join Categorias c on c.id = b.categoria Where c.nombre ='" + libroTraido + "'" , null);

        List<String> arrayLibro = new ArrayList<String>();
        while(c.moveToNext()){
            libroElegido = c.getString(c.getColumnIndex("titulo"));
            arrayLibro.add(libroElegido);
        }

        final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayLibro );
        listLibros.setAdapter(adaptador);

        listLibros.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i =new Intent(LibrosPorCategoria.this,LibroSeleccionado.class);
                startActivity(i);
                String libroVer = (String) listLibros.getItemAtPosition(position);
                i.putExtra("libroVer",libroVer);
                startActivity(i);
            }
        });


    }
}
