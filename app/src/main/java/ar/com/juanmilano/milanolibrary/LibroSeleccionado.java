package ar.com.juanmilano.milanolibrary;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LibroSeleccionado extends AppCompatActivity {

    TextView titulo;
    TextView autor;
    TextView sinopsis;
    TextView fechaPubli;
    TextView editorial;
    TextView cantidadPag;
    TextView categoriaLabel;
    TextView idiomaLabel;
    Button addFav;
    Button delFav;
    String titu;
    String aut;
    String sin;
    String publi;
    String edi;
    String cant;
    String cate;
    String leng;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libro_seleccionado);

        titulo = findViewById(R.id.titulo);
        autor = findViewById(R.id.autor);
        sinopsis = findViewById(R.id.sinopsis);
        fechaPubli = findViewById(R.id.fechaPubli);
        editorial = findViewById(R.id.editorial);
        cantidadPag = findViewById(R.id.cantidadPag);
        addFav = findViewById(R.id.addFav);
        delFav = findViewById(R.id.delFav);
        categoriaLabel = findViewById(R.id.categoriaLabel);
        idiomaLabel = findViewById(R.id.idiomaLabel);



        MiHelperBD helper = new MiHelperBD(this, "Biblioteca", null, 1);



        SQLiteDatabase bd = helper.getReadableDatabase();

        Intent intent = getIntent();
        String libroVer = intent.getStringExtra("libroVer");




        Cursor c = bd.rawQuery("Select * from Biblioteca b inner join Categorias c on b.categoria = c.id Where titulo ='" + libroVer + "'" , null);


        while(c.moveToNext()){
            titu = c.getString(c.getColumnIndex("titulo"));
            titulo.setText(titu);
            aut = c.getString(c.getColumnIndex("autor"));
            autor.setText(aut);
            publi = c.getString(c.getColumnIndex("fechaDePublicacion"));
            fechaPubli.setText(publi);
            edi = c.getString(c.getColumnIndex("editorial"));
            editorial.setText(edi);
            sin = c.getString(c.getColumnIndex("sinopsis"));
            sinopsis.setText(sin);
            cant = c.getString(c.getColumnIndex("cantidadDePag"));
            cantidadPag.setText(cant);
            leng = c.getString(c.getColumnIndex("idioma"));
            idiomaLabel.setText(leng);
            cate = c.getString(c.getColumnIndex("nombre"));
            categoriaLabel.setText(cate);
        }


        }








    public void guardarFav(View v){

        MiHelperBD helper = new MiHelperBD(this, "Biblioteca", null, 1);
        SQLiteDatabase bd = helper.getWritableDatabase();
        Intent intent = getIntent();
        String libroVer = intent.getStringExtra("libroVer");
        String strSQL = "UPDATE Biblioteca SET favorito = 1 WHERE titulo ='" + libroVer + "'";
        bd.execSQL(strSQL);
        bd.close();
        Toast.makeText(this, "El libro se guardo en tu Biblioteca!",
        Toast.LENGTH_SHORT).show();

    }


    public void delFav (View v){

        MiHelperBD helper = new MiHelperBD(this, "Biblioteca", null, 1);
        SQLiteDatabase bd = helper.getWritableDatabase();
        Intent intent = getIntent();
        String libroVer = intent.getStringExtra("libroVer");
        String strSQL = "UPDATE Biblioteca SET favorito = 0 WHERE titulo ='" + libroVer + "'";
        bd.execSQL(strSQL);
        Toast.makeText(this, "El libro se eliminó de tu Biblioteca!",
        Toast.LENGTH_SHORT).show();

    }


    }

