package ar.com.juanmilano.milanolibrary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnBiblioteca;
    Button btnCategorias;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnBiblioteca =(Button) findViewById(R.id.btnBiblioteca);
        btnCategorias = (Button)findViewById(R.id.btnCategorias);

    }


    public void irBiblioteca(View v){
        Intent i = new Intent(this, Biblioteca.class);
        startActivity(i);


    }

    public void verCategorias (View v){
        Intent i = new Intent(this, Categorias.class);
        startActivity(i);

    }



}
