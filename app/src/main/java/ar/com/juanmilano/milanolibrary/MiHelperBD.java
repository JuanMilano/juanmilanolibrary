package ar.com.juanmilano.milanolibrary;

import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MiHelperBD extends SQLiteOpenHelper{
    public MiHelperBD(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);



    }


    public void onCreate(SQLiteDatabase bd) {




        bd.execSQL("CREATE TABLE Categorias(id INTEGER PRIMARY KEY,nombre TEXT)");
        bd.execSQL("INSERT INTO " + "Categorias" + "('id','nombre') VALUES (1,'Ficción');");
        bd.execSQL("INSERT INTO " + "Categorias" + "('id','nombre') VALUES (2,'Fantasía');");
        bd.execSQL("INSERT INTO " + "Categorias" + "('id','nombre') VALUES (3,'Terror');");
        bd.execSQL("INSERT INTO " + "Categorias" + "('id','nombre') VALUES (4,'Novela Literaria');");
        bd.execSQL("INSERT INTO " + "Categorias" + "('id','nombre') VALUES (5,'Deporte');");


        bd.execSQL("CREATE TABLE Biblioteca (id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "titulo TEXT," +
                "autor TEXT, " +
                "sinopsis TEXT," +
                "fechaDePublicacion TEXT, " +
                "editorial TEXT," +
                "cantidadDePag INTEGER," +
                "categoria INTEGER," +
                "idioma TEXT," +
                "favorito INTEGER," +
                "portada TEXT," +
                "FOREIGN KEY (categoria) REFERENCES Categorias(id))");

        ContentValues libro1 = new ContentValues();
        libro1.put("titulo","1984");
        libro1.put("autor","Orwell, George");
        libro1.put("sinopsis","En el año 1984 Londres es una ciudad lúgubre en la que la Policía del Pensamiento controla de forma asfixiante la vida de los ciudadanos. Winston Smith es un peón de este engranaje perverso, su cometido es reescribir la historia para adaptarla a lo que el Partido considera la versión oficial de los hechos... hasta que decide replantearse la verdad del sistema que los gobierna y somete.");
        libro1.put("fechaDePublicacion","8 de junio de 1949");
        libro1.put("editorial","Planeta");
        libro1.put("cantidadDePag",352);
        libro1.put("categoria",1);
        libro1.put("idioma","Español");
        libro1.put("favorito",0);
        libro1.put("portada","1984.jpg");
        bd.insert("Biblioteca", null, libro1);

        ContentValues libro2 = new ContentValues();
        libro2.put("titulo","FUEGO Y SANGRE");
        libro2.put("autor","Martin, George R.R.");
        libro2.put("sinopsis","Siglos antes de que tuvieran lugar los acontecimientos que se relatan en «Canción de hielo y fuego», la casa Targaryen, la única dinastía de señores dragón que sobrevivió a la Maldición de Valyria, se asentó en la isla de Rocadragón. Este es el primero de dos volúmenes, donde el autor de Juego de tronos nos cuenta, con todo lujo de detalles, la historia de tan fascinante familia, empezando por Aegon I Targaryen, creador del icónico Trono de Hierro, y seguido por el resto de las generaciones de Targaryen que lucharon enconadamente por conservar el poder y el trono, hasta la llegada de la guerra civil que estuvo a punto de acabar con ellos");
        libro2.put("fechaDePublicacion","20 de Noviembre de 2018");
        libro2.put("editorial","PLAZA & JANES EDITORES");
        libro2.put("cantidadDePag",880);
        libro2.put("categoria",1);
        libro2.put("idioma","Español");
        libro2.put("favorito",1);
        libro2.put("portada","fuegoysangre.jpg");
        bd.insert("Biblioteca", null, libro2);


        ContentValues libro3 = new ContentValues();
        libro3.put("titulo","IT");
        libro3.put("autor","King, Stephen");
        libro3.put("sinopsis","¿Quién o qué mutila y mata a los niños de un pequeño pueblo norteamericano? ¿Por qué llega cíclicamente el horror a Derry en forma d e un payaso siniestro que va sembrando la destrucción a su paso? Esto es lo que se proponen averiguar los protagonistas de esta novela. Tras veintisiete años de tranquilidad y lejanía una antigua promesa infantil les hace volver al lugar en el que vivieron su infancia y juventud como una terrible pesadilla. ");
        libro3.put("fechaDePublicacion","15 de septiembre de 1986");
        libro3.put("editorial","DEBOLSILLO");
        libro3.put("cantidadDePag",1504);
        libro3.put("categoria",3);
        libro3.put("idioma","Inglés");
        libro3.put("favorito",0);
        libro3.put("portada","it.jpg");
        bd.insert("Biblioteca", null, libro3);

        ContentValues libro4 = new ContentValues();
        libro4.put("titulo","El Resplandor");
        libro4.put("autor","King, Stephen");
        libro4.put("sinopsis"," Esa es la palabra que Danny había visto en el espejo. Y aunque no sabía leer, entendió que era un mensaje de horror. Danny tenía cinco años. Y a esa edad pocos niños saben que los espejos invierten las imágenes y menos aún saben diferenciar entre realidad y fantasía. Pero Danny tenía pruebas de que sus fantasías rel acionadas con el resplandor del espejo acabarían cumpliéndose: REDRUM… MURDER, asesinato");
        libro4.put("fechaDePublicacion","28 de enero de 1977");
        libro4.put("editorial","PLANETA");
        libro4.put("cantidadDePag",656);
        libro4.put("categoria",3);
        libro4.put("idioma","Inglés");
        libro4.put("favorito",0);
        libro4.put("portada","ElResplandor.jpg");
        bd.insert("Biblioteca", null, libro4);


        ContentValues libro5 = new ContentValues();
        libro5.put("titulo","Harry Potter y El Prisionero de Azkaban");
        libro5.put("autor","Rowling, J K");
        libro5.put("sinopsis","El Señor de las Tinieblas está solo y sin amigos, abandonado por sus seguidores. Su vasallo ha estado encadenado doce años. Hoy, antes de la medianoche, el vasallo se liberará e irá a reunirse con su amo. Tras un percance mágico de proporciones gigantescas, Harry Potter huye de la casa de los Dursley y de Little Whinging en el autobús noctámbulo y supone que se enfrentará a duras represalias. Pero el ministro de Magia tiene otros problemas más acuciantes: Sirius Black, el infame prisionero y fiel seguidor de Lord Voldemort, se ha fugado de la prisión de Azkaban.");
        libro5.put("fechaDePublicacion","8 de julio de 1999");
        libro5.put("editorial","SALAMANDRA");
        libro5.put("cantidadDePag",336);
        libro5.put("categoria",2);
        libro5.put("idioma","Español");
        libro5.put("favorito",0);
        libro5.put("portada","hp3.jpg");
        bd.insert("Biblioteca", null, libro5);

        ContentValues libro6 = new ContentValues();
        libro6.put("titulo","Alicia A traves del Espejo");
        libro6.put("autor","Carrol, Lewis");
        libro6.put("sinopsis","A través del espejo y lo que Alicia encontró allí es una novela infantil escrita por Lewis Carroll en 1871.\u200B Es la continuación de Las aventuras de Alicia en el país de las maravillas. Muchas cosas de las que acontecen en el libro parecen, metafóricamente, reflejadas en un espejo.");
        libro6.put("fechaDePublicacion","27 de diciembre de 1871");
        libro6.put("editorial","EDELVIVES");
        libro6.put("cantidadDePag",296);
        libro6.put("categoria",2);
        libro6.put("idioma","Español");
        libro6.put("favorito",0);
        libro6.put("portada","alicia.jpg");
        bd.insert("Biblioteca", null, libro6);

        ContentValues libro7 = new ContentValues();
        libro7.put("titulo","La Voz Ausente");
        libro7.put("autor","Rolon, Gabriel");
        libro7.put("sinopsis","Las cosas no siempre son como parecen, y cuanto más cerca aparenta estar el mundo de cierta calma, mayor suele ser la tormenta que se gesta en lo inespe - rado. Un rayo acaba en segundos con la ensoñación de Pablo Rouviot después de disfrutar de un concierto de violín de una de sus pacientes: su mejor amigo, José, “el Gitano”, fue encontrado al borde de la muerte en su consultorio con un tiro en la cabeza. Todo parece indicar un intento de suicidio.");
        libro7.put("fechaDePublicacion","01 de Noviembre de 2018");
        libro7.put("editorial","Emecé Editores");
        libro7.put("cantidadDePag",544);
        libro7.put("categoria",4);
        libro7.put("idioma","Español");
        libro7.put("favorito",0);
        libro7.put("portada","lavozausente.jpg");
        bd.insert("Biblioteca", null, libro7);

        ContentValues libro8 = new ContentValues();
        libro8.put("titulo","P.D. desde París (Ella y él)");
        libro8.put("autor","Levy, Marc");
        libro8.put("sinopsis","Escrita por Marc Levy, el autor francés vivo más leído de nuestros días, llega esta historia de amor entre una famosa actriz que se esconde en París y un escritor de bestsellers que no deja de mentirse a sí mismo. Ambos sabían que su amistad iba a ser complicada, pero el amor en la Ciudad de la Luz podría encontrar su camino.");
        libro8.put("fechaDePublicacion","11 de septiembre de 2018");
        libro8.put("editorial","PLANETA");
        libro8.put("cantidadDePag",304);
        libro8.put("categoria",4);
        libro8.put("idioma","Francés");
        libro8.put("favorito",0);
        libro8.put("portada","pdparis.jpg");
        bd.insert("Biblioteca", null, libro8);

        ContentValues libro9 = new ContentValues();
        libro9.put("titulo","CREER");
        libro9.put("autor","Simeone, Diego");
        libro9.put("sinopsis","Creer es la fascinante historia de Diego Simeone contada por él mismo. El prestigioso entrenador revela todos sus secretos como líder de grupos y como protagonista de la elite del fútbol mundial. El Cholo desmitifica y confiesa, sin vacilaciones. Transmite con alta intensidad cómo vive el fútbol y la vida");
        libro9.put("fechaDePublicacion","16 de Junio de 2016");
        libro9.put("editorial","Planeta");
        libro9.put("cantidadDePag",333);
        libro9.put("categoria",5);
        libro9.put("idioma","Español");
        libro9.put("favorito",0);
        libro9.put("portada","creer.jpg");
        bd.insert("Biblioteca", null, libro9);

        ContentValues libro10 = new ContentValues();
        libro10.put("titulo","MEXICO 86");
        libro10.put("autor","Maradona, Diego");
        libro10.put("sinopsis","A treinta años de la consagración en México 86, Diego Armando Maradona revisa y relata, con voz inconfundible, el momento más brillante de su carrera, cuando lideró al seleccionado argentino hacia un título del mundo que no se ha repetido hasta hoy. Mirada desde el presente, aquella hazaña histórica alcanza ribetes de leyenda: cómo lo logró, junto a sus compañeros, contra todo y contra todos, narrado en primera persona.");
        libro10.put("fechaDePublicacion","20 de Noviembre de 2016");
        libro10.put("editorial","Sudamericana");
        libro10.put("cantidadDePag",240);
        libro10.put("categoria",5);
        libro10.put("idioma","Español");
        libro10.put("favorito",0);
        libro10.put("portada","maradona.jpg");
        bd.insert("Biblioteca", null, libro10);


    }



    @Override
    public void onUpgrade(SQLiteDatabase bd, int vAnt, int vNueva) {

    }
}
