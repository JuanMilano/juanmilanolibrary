package ar.com.juanmilano.milanolibrary;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca extends AppCompatActivity {

    String biblioteca;
    ListView listBiblioteca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bliblioteca);

        listBiblioteca = findViewById(R.id.listBiblioteca);

        MiHelperBD helper = new MiHelperBD(this, "Biblioteca", null, 1);

        SQLiteDatabase bd = helper.getReadableDatabase();

        Cursor c = bd.rawQuery("Select titulo from Biblioteca Where favorito = 1 ", null);

        List<String> arrayBiblioteca = new ArrayList<String>();
        while(c.moveToNext()){
            biblioteca = c.getString(c.getColumnIndex("titulo"));
            arrayBiblioteca.add(biblioteca);
        }

        final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayBiblioteca );
        listBiblioteca.setAdapter(adaptador);
        listBiblioteca.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i =new Intent(Biblioteca.this,LibroSeleccionado.class);
                startActivity(i);
                String libroVer = (String) listBiblioteca.getItemAtPosition(position);
                i.putExtra("libroVer",libroVer);
                startActivity(i);

            }
        });
    }
}
