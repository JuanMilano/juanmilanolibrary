package ar.com.juanmilano.milanolibrary;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

public class Categorias extends AppCompatActivity {

    String categoria;
    String idCat;
    ListView listCategorias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);

        listCategorias = findViewById(R.id.listCategorias);



        MiHelperBD helper = new MiHelperBD(this, "Categorias", null, 1);

        SQLiteDatabase bd = helper.getReadableDatabase();

        Cursor c = bd.rawQuery("Select nombre from Categorias", null);

        List<String> arrayCategoria = new ArrayList<String>();
        while(c.moveToNext()){
            categoria = c.getString(c.getColumnIndex("nombre"));
            arrayCategoria.add(categoria);
        }

        final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayCategoria );
        listCategorias.setAdapter(adaptador);

        listCategorias.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i =new Intent(Categorias.this,LibrosPorCategoria.class);
                startActivity(i);
                String categoria = (String) listCategorias.getItemAtPosition(position);
                i.putExtra("categoria",categoria);
                startActivity(i);
            }
        });






     }

    }

